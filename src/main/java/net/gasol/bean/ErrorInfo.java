package net.gasol.bean;

public class ErrorInfo<T> {
    public static final Integer OK = 0x01;
    public static final Integer ERROR = 0x02;

    private String message;
    private int code;
    private T data;
    private String url;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
