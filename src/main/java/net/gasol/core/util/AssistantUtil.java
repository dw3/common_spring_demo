package net.gasol.core.util;


import net.gasol.core.Global;
import net.gasol.core.message.ResultMap;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * <font color="green">通用帮助类</font>
 * 
 * @ClassName AssistantUtil
 * @author gasol
 * @date 2016年1月8日 上午10:17:31
 * 
 * @version 1.0
 */

public class AssistantUtil
{
    @SuppressWarnings("unchecked")
    public static Map<String, Object> getUser(HttpSession session)
    {
        return (Map<String, Object>)session.getAttribute(Global.SESSION_USER);
    }
    
    @SuppressWarnings("unchecked")
    public static boolean isExistAccountID(Map<String, Object> user, Integer accountID)
    {
        List<Integer> accountIDs = (List<Integer>)user.get("accountIDs");
        return accountIDs.contains(accountID);
    }
    
    public static void bindUser(Map<String, Object> user, HttpSession session)
    {
        session.setAttribute(Global.SESSION_USER, user);
    }
    
    public static Long getUserID(HttpSession session)
    {
        return (Long)getUser(session).get("id");
    }
    
    public static Map<String, Object> updateResult(int effect)
    {
        if (effect != 1)
        {
            return ResultMap.FAIL_MAP;
        }
        return ResultMap.SUCCESS_MAP;
    }
}
