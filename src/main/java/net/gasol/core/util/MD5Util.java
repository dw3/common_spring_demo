package net.gasol.core.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * <font color="green">加密解密工具类</font>
 * 
 * @ClassName MD5Util
 * @author hongshaowei
 * @date 2016年1月8日 上午10:25:12
 * 
 * @version 1.0
 */

public class MD5Util
{
    private static MD5Util instance = new MD5Util();
    
    private MD5Util()
    {
    }
    
    public static MD5Util getInstance()
    {
        if (null == instance)
            instance = new MD5Util();
        return instance;
    }
    
    /**
     * <font color="red">进行MD5封装</font>
     * 
     * @Title getMD5Code
     * @param inStr
     * @return {@link String}
     * @since 1.0
     * @WorkFlow
     */
    public String getMD5Code(String inStr)
    {
        MessageDigest md5 = null;
        try
        {
            md5 = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
            return "";
        }
        byte[] md5Bytes = md5.digest(inStr.getBytes());
        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++)
        {
            int val = ((int)md5Bytes[i]) & 0xff;
            if (val < 16)
                hexValue.append("0");
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString().toUpperCase();
    }
    
    /**
     * <font color="red">加密解密算法 执行一次加密，两次解密</font>
     * 
     * @Title convertMD5
     * @param inStr
     * @return {@link String}
     * @since 1.0
     * @WorkFlow
     */
    
    public String convertMD5(String inStr)
    {
        char[] a = inStr.toCharArray();
        for (int i = 0; i < a.length; i++)
        {
            a[i] = (char)(a[i] ^ 't');
        }
        return new String(a);
    }
    
    // public static void main(String[] args)
    // {
    // String str = "123456";
    // System.out.println("初始化："+str);
    // String encrypt = MD5Util.getInstance().getMD5Code(str);
    // System.out.println("MD5后:" + encrypt);
    //
    // String decode1 = MD5Util.getInstance().convertMD5(encrypt);
    // String decode2 = MD5Util.getInstance().convertMD5(decode1);
    // System.out.println("再次加密:"+decode1);
    // System.out.println("还原MD5:"+decode2);
    // }
}
