package net.gasol.core.intercept;

import com.alibaba.fastjson.JSON;
import net.gasol.core.Global;
import net.gasol.core.message.ResultMap;
import net.gasol.core.util.AssistantUtil;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * <font color="green">SpringMVC拦截器</font>
 * 
 * @ClassName SecurityInterceptor
 * @author hongshaowei
 * @date 2016年1月12日 下午2:01:39
 * 
 * @version 1.0
 */

public class SecurityInterceptor implements HandlerInterceptor
{
    private static final Logger logger = Logger.getLogger(SecurityInterceptor.class);

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
        throws Exception
    {
    }
    
    @Override
    public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
        throws Exception
    {
    }
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
        throws Exception
    {
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse rep = (HttpServletResponse)response;
        
        // 得到请求路径并解析
        String baseUrl = req.getRequestURI();
        
        if (baseUrl != null)
            baseUrl = baseUrl.replaceFirst(req.getContextPath(), "");
        
        // 获取session中的用户信息
        HttpSession session = req.getSession();
        Map<String, Object> user = AssistantUtil.getUser(session);
        
        Map<String, Object> resultMap = null;
        logger.debug("session:" + user);
        if (user != null)
        {
            // 查询权限
            logger.debug("放行：" + baseUrl);
            return true;
            /*
             * List<String> permission = (List<String>)user.get("permission"); if (permission.contains(baseUrl)) {
             * return true; }
             */
            // resultMap = ResultMap.convertMap(Global.CODE_FAIL, ResultMap.bundle("user.no.permission"));
        }
        else
        {
            logger.debug("拦截路径：" + baseUrl);
            resultMap = ResultMap.convertMap(Global.CODE_NO_LOGIN, ResultMap.bundle("user.no.login"));
        }
        
        rep.setCharacterEncoding("UTF-8");
        rep.setContentType("application/json");
        rep.getWriter().write(JSON.toJSONString(resultMap));
        rep.getWriter().flush();
        return false;
    }
}
