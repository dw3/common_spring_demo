package net.gasol.core.intercept;

import com.alibaba.fastjson.JSON;
import net.gasol.core.Global;
import net.gasol.core.annotation.Length;
import net.gasol.core.annotation.Range;
import net.gasol.core.annotation.Rule;
import net.gasol.core.annotation.Validation;
import net.gasol.core.message.ResultMap;
import net.gasol.core.message.ValidMessage;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.*;

@Aspect // FOR AOP
@Component
public class ValidInterceptor {
//    2.1 @Aspect
//    作用是把当前类标识为一个切面供容器读取
//2.2 @Before
//    标识一个前置增强方法，相当于BeforeAdvice的功能，相似功能的还有
//2.3 @AfterReturning
//    后置增强，相当于AfterReturningAdvice，方法正常退出时执行
//2.4 @AfterThrowing
//    异常抛出增强，相当于ThrowsAdvice
//2.5 @After
//    final增强，不管是抛出异常或者正常退出都会执行
//2.6 @Around
//    环绕增强，相当于MethodInterceptor
//2.7 @DeclareParents
//    引介增强，相当于IntroductionInterceptor


    public static ThreadLocal<String> threadLocal = new ThreadLocal<>();
    private Logger logger = Logger.getLogger(getClass());

    @Pointcut("execution(* net.gasol.controller..*.*(..))&& @annotation(org.springframework.web.bind.annotation.RequestMapping)")
    public void webAspect() {

    }

    @Before("webAspect()")
    public void beforeTest(JoinPoint joinPoint) throws Throwable {
        threadLocal.set(System.currentTimeMillis() + "");

        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        // 记录下请求内容
        logger.info("URL : " + request.getRequestURL().toString());
        logger.info("HTTP_METHOD : " + request.getMethod());
        logger.info("IP : " + request.getRemoteAddr());
        logger.info("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        logger.info("ARGS : " + Arrays.toString(joinPoint.getArgs()));
    }


    @AfterReturning(returning = "ret", pointcut = "webAspect()")
    public void doAfterReturning(Object ret) throws Throwable {
        // 处理完请求，返回内容
        logger.info("RESPONSE : " + ret);
        logger.info("SPEND TIME : " + (System.currentTimeMillis() - Long.valueOf(threadLocal.get())));
    }

    /**
     * 拦截器具体实现
     *
     * @param pjp
     * @return JsonResult（被拦截方法的执行结果，或需要登录的错误提示。）
     */
    @Around("webAspect()") //指定拦截器规则；也可以直接把“execution(* com.xjj.........)”写进这里
    public Object Interceptor(ProceedingJoinPoint pjp) throws Throwable {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod(); //获取被拦截的方法
        HttpServletResponse response = null;
        Map<String, Object> params = null;
        HttpServletRequest request;
        if (method.isAnnotationPresent(Validation.class)) {
            Validation valid = method.getAnnotation(Validation.class);
            Object[] args = pjp.getArgs();
            for (Object obj : args) {
                if (obj instanceof HttpServletResponse) {
                    response = (HttpServletResponse) obj;
                } else if (obj instanceof Map) {
                    params = (Map<String, Object>) obj;
                } else if (obj instanceof HttpServletRequest) {
                    request = (HttpServletRequest) obj;
                }
            }
            if (!valid(valid, params)) {
                response.setCharacterEncoding("UTF-8");
                response.setContentType("application/json");
                response.getWriter().write(JSON.toJSONString(ResultMap.convertMap(Global.CODE_PARAMETER_ERROR, null, threadLocal.get())));
                return null;
            }
        }
        return pjp.proceed();
    }

    private boolean valid(Validation valid, Map<String, Object> params) {
        return notNull(valid.notNull(), params) && length(valid.length(), params) && email(valid.email(), params) && rules(valid.rules(), params)
                && mobile(valid.mobile(), params) && number(valid.number(), params) && range(valid.range(), params);
    }

    /**
     * <font color="red">数字区间校验</font>
     *
     * @param ranges
     * @param params
     * @return
     * @Title range
     * @WorkFlow
     * @since
     */
    private boolean range(Range[] ranges, Map<String, Object> params) {
        if (ranges != null && ranges.length > 0) {
            for (Range range : ranges) {
                String filed = range.filed();
                int min = range.min();
                int max = range.max();
                Object param = params.get(filed);
                if (param != null && (Integer.parseInt(param.toString()) < min || Integer.parseInt(param.toString()) > max)) {
                    threadLocal.set(ValidMessage.bundle("validator.constraints.Range.message", filed, min, max));

                    return false;
                }
            }
        }
        return true;
    }

    /**
     * <font color="red">数字校验</font>
     *
     * @param numbers
     * @param params
     * @return
     * @Title number
     * @WorkFlow
     * @since
     */
    private boolean number(String[] numbers, Map<String, Object> params) {
        if (numbers != null && numbers.length > 0) {
            for (String filed : numbers) {
                Object param = params.get(filed);
                if (param != null && !param.toString().matches(ValidMessage.bundle("number"))) {
                    threadLocal.set(ValidMessage.bundle("validator.constraints.Number.message", filed));

                    return false;
                }
            }
        }
        return true;
    }

    /**
     * <font color="red">邮箱校验</font>
     *
     * @param emails
     * @param params
     * @return
     * @Title email
     * @WorkFlow
     * @since
     */
    private boolean email(String[] emails, Map<String, Object> params) {
        if (emails != null && emails.length > 0) {
            for (String filed : emails) {
                Object param = params.get(filed);
                if (param != null && !param.toString().matches(ValidMessage.bundle("email"))) {
                    threadLocal.set(ValidMessage.bundle("validator.constraints.Email.message", filed));

                    return false;
                }
            }
        }
        return true;
    }

    /**
     * <font color="red">手机校验</font>
     *
     * @param mobile
     * @param params
     * @return
     * @Title mobile
     * @WorkFlow
     * @since
     */
    private boolean mobile(String[] mobile, Map<String, Object> params) {
        if (mobile != null && mobile.length > 0) {
            for (String filed : mobile) {
                Object param = params.get(filed);
                if (param != null && !param.toString().matches(ValidMessage.bundle("mobile"))) {
                    threadLocal.set(ValidMessage.bundle("validator.constraints.Mobile.message", filed));

                    return false;
                }
            }
        }
        return true;
    }

    /**
     * <font color="red">非空校验</font>
     *
     * @param notNull
     * @param params
     * @return
     * @Title notNull
     * @WorkFlow
     * @since
     */
    private boolean notNull(String[] notNull, Map<String, Object> params) {
        if (notNull != null && notNull.length > 0) {
            for (String filed : notNull) {
                Object param = params.get(filed);
                if (param == null || param.toString().matches(ValidMessage.bundle("isNull"))) {
                    threadLocal.set(ValidMessage.bundle("validator.constraints.IsNull.message", filed));

                    return false;
                }
            }
        }
        return true;
    }

    /**
     * <font color="red">长度校验</font>
     *
     * @param lengths
     * @param params
     * @return
     * @Title length
     * @WorkFlow
     * @since
     */
    private boolean length(Length[] lengths, Map<String, Object> params) {
        if (lengths != null && lengths.length > 0) {
            for (Length length : lengths) {
                String filed = length.filed();
                int min = length.min();
                int max = length.max();
                Object param = params.get(filed);
                if (param != null && (param.toString().length() < min || param.toString().length() > max)) {
                    threadLocal.set(ValidMessage.bundle("validator.constraints.Length.message", filed, min, max));

                    return false;
                }
            }
        }
        return true;
    }

    /**
     * <font color="red">自定义校验规则</font>
     *
     * @param rules
     * @param params
     * @return
     * @Title rules
     * @WorkFlow
     * @since
     */
    private boolean rules(Rule[] rules, Map<String, Object> params) {
        if (rules != null && rules.length > 0) {
            for (Rule rule : rules) {
                String filed = rule.filed();
                String regx = rule.regx();
                Object param = params.get(filed);
                if (param != null && !param.toString().matches(regx)) {
                    threadLocal.set(ValidMessage.bundle("validator.constraints.Rule.message", filed));

                    return false;
                }
            }
        }
        return true;
    }
}