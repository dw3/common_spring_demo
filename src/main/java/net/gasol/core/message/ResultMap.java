package net.gasol.core.message;


import net.gasol.core.Global;

import java.text.MessageFormat;
import java.util.*;

/**
 * <font color="green">返回数据格式封装类</font>
 *
 * @author hongshaowei
 * @version 1.0
 * @ClassName ResultMap
 * @date 2015年4月24日 上午10:15:20
 */
public class ResultMap {
    /**
     * @Fields resourceBundle : 绑定资源
     */
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("config/app/result_message");

    /**
     * @Fields SUCCESS_MAP : 返回成功，不包含数据
     */
    public static final Map<String, Object> SUCCESS_MAP = convertMap(Global.CODE_SUCCESS, bundle(Global.CODE_SUCCESS.toString()));

    /**
     * @Fields FAIL_MAP : 返回失败，不包含数据
     */
    public static final Map<String, Object> FAIL_MAP = convertMap(Global.CODE_FAIL, bundle(Global.CODE_FAIL.toString()));

    /**
     * @Fields ERROR_MAP : 返回错误，不包含数据
     */
    public static final Map<String, Object> ERROR_MAP = convertMap(Global.CODE_SYSTEM_ERROR, bundle(Global.CODE_SYSTEM_ERROR.toString()));

    /**
     * @Fields EMPTY_MAP : 返回数据为空
     */
    public static final Map<String, Object> EMPTY_MAP = convertMap(Global.CODE_DATA_EMPTY, bundle(Global.CODE_DATA_EMPTY.toString()));

    /**
     * token 验证出错
     */
    public static final Map<String, Object> TOKEN_ERROR = convertMap(Global.PERMISSION_DENIED, bundle(Global.PERMISSION_DENIED.toString()));

    /**
     * <font color="red">返回json：只包含状态码</font>
     *
     * @param stateCode
     * @return
     * @Title convertMap
     * @WorkFlow
     * @since
     */
    public static Map<String, Object> convertMap(Integer stateCode) {
        return convertMap(stateCode, null, null);
    }

    /**
     * <font color="red">返回json：包含状态码、数据</font>
     *
     * @param stateCode
     * @param data
     * @return
     * @Title convertMap
     * @WorkFlow
     * @since
     */
    public static Map<String, Object> convertMap(Integer stateCode, Object data) {
        return convertMap(stateCode, data, null);
    }

    /**
     * <font color="red">返回json：包含状态码、提示信息</font>
     *
     * @param stateCode
     * @param message
     * @return
     * @Title convertMap
     * @WorkFlow
     * @since
     */
    public static Map<String, Object> convertMap(Integer stateCode, String message) {
        return convertMap(stateCode, null, message);
    }

    /**
     * <font color="red">返回json：包含状态码、数据、提示信息</font>
     *
     * @param stateCode
     * @param data
     * @param message
     * @return
     * @Title convertMap
     * @WorkFlow
     * @since
     */
    public static Map<String, Object> convertMap(Integer stateCode, Object data, String message) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("stateCode", stateCode);
        map.put("data", data);
        map.put("message", message);
        return map;
    }

    /**
     * <font color="red">返回成功信息：通用，包含数据</font>
     *
     * @param data
     * @return
     * @Title getSuccessMap
     * @WorkFlow
     * @since
     */
    public static Map<String, Object> getSuccessMap(Object data) {
        return convertMap(Global.CODE_SUCCESS, data, bundle(Global.CODE_SUCCESS.toString()));
    }

    /**
     * <font color="red">返回失败信息：通用</font>
     *
     * @param message
     * @return
     * @Title getFailMap
     * @WorkFlow
     * @since
     */
    public static Map<String, Object> getFailMap(String message) {
        return convertMap(Global.CODE_FAIL, message);
    }

    /**
     * <font color="red">返回系统错误：通用</font>
     *
     * @param message
     * @return
     * @Title getErrorMap
     * @WorkFlow
     * @since
     */
    public static Map<String, Object> getErrorMap(String message) {
        return convertMap(Global.CODE_SYSTEM_ERROR, message);
    }

    /**
     * <font color="red">返回分页数据：通用</font>
     *
     * @param listMap
     * @param total
     * @return
     * @Title getListResult
     * @WorkFlow
     * @since
     */
    public static Map<String, Object> getListResult(List<Map<String, Object>> listMap, Object total) {
        Map<String, Object> result = new HashMap<>();
        result.put("rows", listMap);
        result.put("total", total);
        return getSuccessMap(result);
    }

    /**
     * <font color="red">无占位符的字符资源</font>
     *
     * @param key
     * @return {@linkplain String}
     * @Title bundle
     * @since 1.0
     */
    public static String bundle(String key) {
        return resourceBundle.getString(key);
    }

    /**
     * <font color="red">有占位符的字符资源</font>
     *
     * @param key
     * @param arguments
     * @return {@linkplain String}
     * @Title bundle
     * @since 1.0
     */
    public static String bundle(String key, Object... arguments) {
        return MessageFormat.format(bundle(key), arguments);
    }

}
