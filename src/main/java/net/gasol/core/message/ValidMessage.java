package net.gasol.core.message;

import java.text.MessageFormat;
import java.util.ResourceBundle;

/**
 * <font color="green">校验资源绑定</font>
 * 
 * @ClassName BaseMessage
 * @author hongshaowei
 * @date 2015年4月21日 上午11:05:41
 * 
 * @version 1.0
 */
public class ValidMessage
{
    /**
     * @Fields resourceBundle : 绑定资源
     */
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("config/app/validation_message");
    
    /**
     * <font color="red">无占位符的字符资源</font>
     * 
     * @Title bundle
     * @param key
     * @return {@linkplain String}
     * @since 1.0
     */
    public static String bundle(String key)
    {
        return resourceBundle.getString(key);
    }
    
    /**
     * <font color="red">有占位符的字符资源</font>
     * 
     * @Title bundle
     * @param key
     * @param arguments
     * @return {@linkplain String}
     * @since 1.0
     */
    public static String bundle(String key, Object... arguments)
    {
        return MessageFormat.format(resourceBundle.getString(key), arguments);
    }
    
    // public static void main(String[] args)
    // {
    // System.out.println("402411352@qq.com".matches(bundle("email")));
    // }
}
