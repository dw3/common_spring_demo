package net.gasol.core.common;

import tk.mybatis.mapper.common.BaseMapper;
import tk.mybatis.mapper.common.MySqlMapper;

public interface IMapper<T> extends BaseMapper<T> ,MySqlMapper<T> {
}
