package net.gasol.core.common;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public abstract class ServiceAdapter<T> implements IService<T> {
    public abstract IMapper getMapper();

    @Override
    public T selectOne(T record) {
        return (T) getMapper().selectOne(record);
    }

    @Override
    public List<T> select(T record) {
        return getMapper().select(record);
    }

    @Override
    public List<T> selectAll() {
        return getMapper().selectAll();
    }

    @Override
    public int selectCount(T record) {
        return getMapper().selectCount(record);
    }

    @Override
    public T selectByPrimaryKey(Object key) {
        return (T) getMapper().selectByPrimaryKey(key);
    }

    @Override
    public boolean existsWithPrimaryKey(Object key) {
        return getMapper().existsWithPrimaryKey(key);
    }

    @Transactional
    @Override
    public int insert(T record) {
        return getMapper().insert(record);
    }

    @Transactional
    @Override
    public int insertSelective(T record) {
        return getMapper().insertSelective(record);
    }

    @Transactional
    @Override
    public int updateByPrimaryKey(T record) {
        return getMapper().updateByPrimaryKey(record);
    }

    @Transactional
    @Override
    public int updateByPrimaryKeySelective(T record) {
        return getMapper().updateByPrimaryKeySelective(record);
    }

    @Transactional
    @Override
    public int delete(T record) {
        return getMapper().delete(record);
    }

    @Transactional
    @Override
    public int deleteByPrimaryKey(Object key) {
        return getMapper().deleteByPrimaryKey(key);
    }

    @Transactional
    @Override
    public int insertList(List<T> recordList) {
        return getMapper().insertList(recordList);
    }

    @Transactional
    @Override
    public int insertUseGeneratedKeys(T record) {
        return getMapper().insertUseGeneratedKeys(record);
    }
}
