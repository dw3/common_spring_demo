package net.gasol.core;

/**
 * <font color="green">全局系统变量</font>
 * 
 * @ClassName Global
 * @author gasol
 * @date 2015年4月21日 上午11:01:17
 * 
 * @version 1.0
 */
public class Global
{
    /**
     * @Fields SESSION_USER : 用户信息session键值
     */
    public final static String SESSION_USER = "system.user";
    
    /**
     * @Fields SESSION_CODE : 图片验证码
     */
    public final static String SESSION_VALIDCODE = "img.code";
    
    /**
     * @Fields CODE_SUCCESS : 成功状态码
     */
    public final static Integer CODE_SUCCESS = 200;
    
    /**
     * @Fields CODE_FAIL : 失败状态码
     */
    public final static Integer CODE_FAIL = 201;
    
    /**
     * @Fields CODE_SIGNATURE_ERROR : 签名错误状态码
     */
    public final static Integer CODE_SIGNATURE_ERROR = 202;
    
    /**
     * @Fields CODE_DATA_EMPTY : 数据为空状态码
     */
    public final static Integer CODE_DATA_EMPTY = 203;
    
    /**
     * @Fields CODE_PARAMETER_ERROR : 参数有误状态码
     */
    public final static Integer CODE_PARAMETER_ERROR = 204;
    
    /**
     * @Fields CODE_NO_LOGIN : 未登录状态码
     */
    public final static Integer CODE_NO_LOGIN = 205;
    
    /**
     * @Fields code_system_error : 系统错误状态码
     */
    public final static Integer CODE_SYSTEM_ERROR = 500;

    /**
     * permission_denied :  token验证失败
     */
    public static final Integer PERMISSION_DENIED = 210;
}
