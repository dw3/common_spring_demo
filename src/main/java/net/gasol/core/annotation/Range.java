package net.gasol.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <font color="green">数字范围验证</font>
 * 
 * @ClassName Length
 * @author gasol
 * @date 2015年4月24日 上午9:40:34
 * 
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.ANNOTATION_TYPE })
public @interface Range {
	/**
	 * <font color="red">校验字段</font>
	 * 
	 * @Title filed
	 * @return String
	 * @since 1.0
	 */
	public String filed();

	/**
	 * <font color="red">最小值</font>
	 * 
	 * @Title min
	 * @return int
	 * @since 1.0
	 */
	public int min();

	/**
	 * <font color="red">最小值</font>
	 * 
	 * @Title max
	 * @return int
	 * @since 1.0
	 */
	public int max();

}
