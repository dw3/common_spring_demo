package net.gasol.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <font color="green">非实体校验注解</font>
 * 
 * @ClassName Validation
 * @author gasol
 * @date 2015年4月24日 上午9:34:08
 * 
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
public @interface Validation {
	/**
	 * <font color="red">非空验证</font>
	 * 
	 * @Title notNull
	 * @return String[]
	 * @since 1.0
	 */
	public String[] notNull() default {};

	/**
	 * <font color="red">邮箱验证</font>
	 * 
	 * @Title email
	 * @return String[]
	 * @since 1.0
	 */
	public String[] email() default {};

	/**
	 * <font color="red">手机验证</font>
	 * 
	 * @Title mobile
	 * @return String[]
	 * @since 1.0
	 */
	public String[] mobile() default {};

	/**
	 * <font color="red">自定义验证</font>
	 * 
	 * @Title rules
	 * @return {@linkplain Rule}
	 * @since 1.0
	 */
	public Rule[] rules() default {};

	/**
	 * <font color="red">长度验证</font>
	 * 
	 * @Title length
	 * @return {@linkplain Length}
	 * @since 1.0
	 */
	public Length[] length() default {};

	/**
	 * <font color="red">数字验证</font>
	 * 
	 * @Title number
	 * @return String[]
	 * @since 1.0
	 * @WorkFlow
	 */
	public String[] number() default {};
	
	public Range[] range() default{};
}
