package net.gasol.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <font color="green">自定义校验规则</font>
 * 
 * @ClassName Rule
 * @author gasol
 * @date 2015年4月24日 上午9:38:44
 * 
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.ANNOTATION_TYPE })
public @interface Rule {
	/**
	 * <font color="red">校验字段</font>
	 * 
	 * @Title filed
	 * @return String
	 * @since 1.0
	 */
	public String filed();

	/**
	 * <font color="red">校验正则表达式</font>
	 * 
	 * @Title regx
	 * @return String
	 * @since 1.0
	 */
	public String regx();
}
