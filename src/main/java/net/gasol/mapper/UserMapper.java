package net.gasol.mapper;

import net.gasol.core.common.IMapper;
import net.gasol.bean.User;

public interface UserMapper extends IMapper<User> {
}
