package net.gasol.mapper;


import net.gasol.core.common.IMapper;
import net.gasol.bean.Test;

public interface TestMapper extends IMapper<Test> {
}
