package net.gasol.mapper;

import net.gasol.bean.SysUser;
import net.gasol.core.common.IMapper;

import java.util.Map;

public interface SysUserMapper extends IMapper<SysUser> {
    SysUser getSysUserByUsername(String username);
}
