package net.gasol.controller;

import net.gasol.bean.Msg;
import net.gasol.util.L;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@ApiIgnore
public class WebController {
    @RequestMapping("/log")
    public String index() {
        String msg = "Spring Boot系列之Log4j2的配置及使用";
        L.t(msg);
        L.d(msg);
        L.i(msg);
        L.w(msg);
        L.e(msg);
        return "index";
    }
    @RequestMapping("/")
    public String index(Model model){
        Msg msg =  new Msg("测试标题","测试内容","额外信息，只对管理员显示");
        model.addAttribute("msg", msg);
        return "home";
    }
}