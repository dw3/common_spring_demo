package net.gasol.controller;

import net.gasol.core.annotation.Length;
import net.gasol.core.annotation.Validation;
import net.gasol.core.message.ResultMap;
import net.gasol.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/gasol/user")
@ApiIgnore
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping(value = "/regist")
    @Validation(number = {"username"}, notNull = {"password"}, length = {@Length(filed = "username", min = 1, max = 3)})
    public Map<String, Object> regist(@RequestParam Map<String, Object> params, HttpServletRequest request, HttpServletResponse response) {
        String userName = (String) params.get("username");
        System.out.println("userName:" + userName);
        return null;
    }

    @RequestMapping("/login")
    public void login(@RequestParam Map<String, Object> params) {

    }

    @RequestMapping("/getList")
    public Map<String, Object> getUsers() {
        List list = userService.selectAll();
        return ResultMap.getSuccessMap(list);
    }

}
