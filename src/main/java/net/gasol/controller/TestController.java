package net.gasol.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.gasol.bean.Msg;
import net.gasol.bean.Test;
import net.gasol.core.exception.MyException;
import net.gasol.core.message.ResultMap;
import net.gasol.service.impl.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Random;

@RestController
@RequestMapping(value = "/gasol/test")
public class TestController {

    @Autowired
    TestService testService;


    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public Map<String, Object> insert() {
        Test test = new Test();
        int i = new Random().nextInt(100);
        test.setId(i);
        test.setAge(i);
        test.setAddress("address:" + i);
        test.setName("name:" + i);
        if (testService.insert(test) > 0) {
            return ResultMap.SUCCESS_MAP;
        } else {
            return ResultMap.FAIL_MAP;
        }
    }

    @RequestMapping(value = "/getList", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getList() {
        List list = testService.selectAll();
        return ResultMap.getSuccessMap(list);
    }

    @RequestMapping(value = "/testHeader", method = RequestMethod.GET)
    public void testHeader(@RequestHeader String token) {
    }


    @RequestMapping(value = "/testSwagger", method = RequestMethod.POST)
    @ApiOperation(value = "测试swagger", notes = "notes")
    public Map<String, Object> testSwagger(@RequestParam @ApiParam() Map<String, Object> params,
                                           @RequestParam @ApiParam String username, @RequestParam @ApiParam int password) {
        return ResultMap.SUCCESS_MAP;
    }

    @RequestMapping(value = "/testError", method = RequestMethod.POST)
    public String testError() throws Exception {
        throw new Exception("发生错误");
    }

    @RequestMapping(value = "/testError2", method = RequestMethod.POST)
    public String testErrorTwo() throws Exception {
        throw new MyException("发生错误2");
    }

}
