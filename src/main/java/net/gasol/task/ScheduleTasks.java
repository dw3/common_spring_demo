package net.gasol.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ScheduleTasks {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat();

    @Scheduled(fixedRate = 5000)
    public void reportCurrentTime() {
        System.out.println("current time:" + dateFormat.format(new Date()));
    }
}
