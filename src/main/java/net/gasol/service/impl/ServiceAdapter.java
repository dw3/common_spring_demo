package net.gasol.service.impl;

import net.gasol.core.common.IMapper;
import net.gasol.core.common.IService;

import java.util.List;

public abstract class ServiceAdapter<T> implements IService<T> {
    abstract IMapper<T> getMapper();

    @Override
    public T selectOne(T record) {
        return getMapper().selectOne(record);
    }

    @Override
    public List<T> select(T record) {
        return getMapper().select(record);
    }

    @Override
    public List<T> selectAll() {
        return getMapper().selectAll();
    }

    @Override
    public int selectCount(T record) {
        return getMapper().selectCount(record);
    }

    @Override
    public T selectByPrimaryKey(Object key) {
        return getMapper().selectByPrimaryKey(key);
    }

    @Override
    public boolean existsWithPrimaryKey(Object key) {
        return getMapper().existsWithPrimaryKey(key);
    }

    @Override
    public int insert(T record) {
        return getMapper().insert(record);
    }

    @Override
    public int insertSelective(T record) {
        return getMapper().insertSelective(record);
    }

    @Override
    public int updateByPrimaryKey(T record) {
        return getMapper().updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(T record) {
        return getMapper().updateByPrimaryKeySelective(record);
    }

    @Override
    public int delete(T record) {
        return getMapper().delete(record);
    }

    @Override
    public int deleteByPrimaryKey(Object key) {
        return getMapper().deleteByPrimaryKey(key);
    }

    @Override
    public int insertList(List<T> recordList) {
        return getMapper().insertList(recordList);
    }

    @Override
    public int insertUseGeneratedKeys(T record) {
        return getMapper().insertUseGeneratedKeys(record);
    }
}
