package net.gasol.service.impl;

import net.gasol.core.common.IMapper;
import net.gasol.core.common.ServiceAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class TestService extends ServiceAdapter {
    @Autowired
    IMapper testMapper;

    @Override
    public IMapper getMapper() {
        return testMapper;
    }
}
