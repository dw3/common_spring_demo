package net.gasol.service.impl;

import net.gasol.bean.SysRole;
import net.gasol.bean.SysUser;
import net.gasol.core.common.IMapper;
import net.gasol.core.common.IService;
import net.gasol.mapper.SysUserMapper;
import net.gasol.service.iface.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class SysUserService extends ServiceAdapter implements UserDetailsService, ISysUserService {
    @Autowired
    SysUserMapper sysUserMapper;

    @Override
    IMapper getMapper() {
        return sysUserMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser sysUser = getUserByUsername(username);
        if (sysUser == null) {
            throw new UsernameNotFoundException("用户名不存在");
        }
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        //用于添加用户的权限。只要把用户权限添加到authorities 就万事大吉。
        for (SysRole role : sysUser.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
            System.out.println(role.getName());
        }
        return new org.springframework.security.core.userdetails.User(sysUser.getUsername(),
                sysUser.getPassword(), authorities);
    }

    @Override
    public SysUser getUserByUsername(String username) {
        return sysUserMapper.getSysUserByUsername(username);
    }


}
