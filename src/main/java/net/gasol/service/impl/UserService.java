package net.gasol.service.impl;

import net.gasol.core.common.IMapper;
import net.gasol.bean.User;
import net.gasol.core.common.ServiceAdapter;
import net.gasol.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @version 1.0
 * @date 2017/11/27
 * @Author gasol
 * @className UserService
 */
@Service
public class UserService extends ServiceAdapter<User> {
    @Autowired
    UserMapper userMapper;

    @Override
    public IMapper getMapper() {
        return userMapper;
    }
}
