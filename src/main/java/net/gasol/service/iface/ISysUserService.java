package net.gasol.service.iface;

import net.gasol.bean.SysUser;

public interface ISysUserService {
    SysUser getUserByUsername(String username);
}
