package net.gasol;

import net.gasol.core.common.IMapper;
import net.gasol.core.intercept.TokenAuthorFilter;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Aswords
 * @Date 2017/2/13
 * @since v0.1
 */
@SpringBootApplication
@MapperScan(basePackages = "net.gasol.mapper", markerInterface = IMapper.class)
@EnableConfigurationProperties
@PropertySources({@PropertySource("classpath:/config/app/app-common.properties"),
        @PropertySource("classpath:/config/app/app-${spring.profiles.active}.properties")})
//@EnableScheduling  //允许定时任务
public class CommonApplication {


    public static void main(String[] args) throws Exception {
        //默认配置
        if (System.getProperty("spring.profiles.active") == null) {
            System.setProperty("spring.profiles.active", "dev");
        }
        new SpringApplicationBuilder(CommonApplication.class).web(true).run(args);
    }



//    @Bean
//    public FilterRegistrationBean filterRegistrationBean() {
//        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
//        TokenAuthorFilter httpBasicFilter = new TokenAuthorFilter();
//        registrationBean.setFilter(httpBasicFilter);
//        List<String> urlPatterns = new ArrayList<String>();
//        urlPatterns.add("/gasol/*");
//        registrationBean.setUrlPatterns(urlPatterns);
//        return registrationBean;
//    }
}
